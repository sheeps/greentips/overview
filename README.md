# GreenTips

Mobile web application to foster the ecological tip exchanges between humans.

### Connection

![Connection](./media/screen.jpg)

### Tips feed

Could be either:

- video
- photo
- or just raw text

Each tip is tagged, dated and associated with a reputation score based on the vote of other users.

![Tips feed](./media/screen2.jpg)

![Tips feed 2](./media/screen3.jpg)

![Tips feed 3](./media/screen8.jpg)

### News feed

The articles aim to raise people's awareness of threats and risks involved in major environmental topics.

![News feed](./media/screen4.jpg)

### User profile

The profile summarizes the contribution of a user and the community he joined.

![User profile](./media/screen5.jpg)

![User profile 2](./media/screen6.jpg)

![User profile 3](./media/screen7.jpg)
